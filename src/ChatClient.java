import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {
    private int port;
    private String address;
    private Socket server;
    public ChatClient(String address,int port) {
        this.port=port;
        this.address=new String(address);
        try {
            this.server=new Socket(address,port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void sendMessage(String message)
    {
        try {
            PrintWriter kaServeru=new PrintWriter(server.getOutputStream(),true);
            kaServeru.println(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public String getMessage()
    {
        try {
            BufferedReader odServera=new BufferedReader(new InputStreamReader(server.getInputStream()));
            if (odServera.ready())
            {
                return odServera.readLine();
            }
            return null;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }


    }
}
