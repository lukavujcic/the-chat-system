import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Map;

public class ClientThread implements Runnable {
    private Socket socket = null;
    private Map<Integer, Socket> imenik=null;
    private int br;


    public ClientThread(Socket socket, int br, Map<Integer, Socket> imenik) {
        this.socket = socket;
        this.imenik = imenik;
        this.br=br;
    }

    @Override
    public void run() {
       System.out.println(br);
            try {
                BufferedReader odSoketa = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter pw=new PrintWriter(socket.getOutputStream(),true);
                pw.println(br);
               // System.out.println("!");
                while (true) {
                    String s = odSoketa.readLine();

               //     System.out.println(s);

                    //Integer br=Integer.parseInt(s);
                    String str[]=s.split("[;]");
                    Socket socketPrimaoca = imenik.get(Integer.parseInt(str[0]));
                    String poruka=str[1];
                    PrintWriter  kaSoketu=new PrintWriter(socketPrimaoca.getOutputStream(),true);

                    kaSoketu.println(br+" : "+poruka);

                  //  System.out.println(s);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

    }
}
