import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;

public class ChatSystem extends Application {

    public static void main(String[] args) {
        launch(args);
    }
   private ChatClient client=new ChatClient("localhost",3000);
    @Override
    public void start(Stage primaryStage) {
        VBox root=new VBox(30);
        root.setPadding(new Insets(30,30,30,30));
        TextArea messageArea=new TextArea();
        Label lblID=new Label("");
        Label lblPoruka=new Label("Poruka");
        Label lblKome=new Label("Kome: ");
        Button btnPosalji=new Button("Posalji");
        TextField taKome=new TextField();
        TextField taPoruka=new TextField();
        root.getChildren().addAll(lblID,messageArea,lblKome,taKome,lblPoruka,taPoruka,btnPosalji);
        Scene scene=new Scene(root,800,600);
        primaryStage.setScene(scene);
        primaryStage.setTitle("The Chat System");
        lblID.setText("ID: "+client.getMessage());
        messageArea.setEditable(false);
        btnPosalji.setOnAction((event)->{
            client.sendMessage(taKome.getText()+";"+taPoruka.getText());
            messageArea.appendText("Ja ka ("+taKome.getText()+"): " +taPoruka.getText()+"\n");
            taKome.setText("");
            taPoruka.setText("");
        });

        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(100),
                ae -> {
                    String message=client.getMessage();
                    if (message!=null)
                    {
                        messageArea.appendText(message+"\n");
                    }

                }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
        primaryStage.show();
    }
}
