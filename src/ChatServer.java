import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Hashtable;
import java.util.Map;

public class ChatServer {
    private  int port;
    private  int br=1;
    Map<Integer,Socket> imenik=new Hashtable<>(); //klijenti u sistemu imaju jedinstveni ID, mapu koristimo da preslikamo ID u Sokete
    public ChatServer(int port) {
        this.port=port;
        try {
            ServerSocket serverSocket=new ServerSocket(port);
            while(true)
            {
                Socket socket=serverSocket.accept();
                imenik.put(br,socket);
                Thread t=new Thread(new ClientThread(socket,br,imenik));
                t.start();
                br++;

            }
        } catch (IOException e) {
                e.printStackTrace();
        }
    }
}
